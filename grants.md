# Grants

## Active

- [ACME-011](grants/ACME-011.pdf) Web3/MetaMask integration
  - Status as of June 4: ~30% complete, testing on the testnet, ETA end of August
- [ACME-013](grants/ACME-013.pdf) Explorer maintenance (Jul-Sep 2023)

## Completed

- [ACME-010](grants/ACME-010.pdf) Explorer maintenance (Mar-Jun 2023)